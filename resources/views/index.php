<!-- resources/views/index.php -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <base href="/">
    <title>User Test Demo</title>
    <link rel="stylesheet" type="text/css" href="bower_components/angular-material/angular-material.css">
    <link rel="stylesheet" type="text/css" href="bower_components/angular-ui-grid/ui-grid.min.css">
    <link rel="stylesheet" type="text/css" href="app.css">
    <!-- Application Dependencies -->
    <script src="bower_components/angular/angular.js"></script>
    <script src="bower_components/angular-ui-router/release/angular-ui-router.js"></script>
    <script src="bower_components/satellizer/satellizer.js"></script>
    <script src="bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="bower_components/angular-aria/angular-aria.min.js"></script>
    <script src="bower_components/angular-messages/angular-messages.min.js"></script>
    <script src="bower_components/angular-material/angular-material.js"></script>
    <script src="bower_components/angular-ui-grid/ui-grid.min.js"></script>

    <!-- Application Main Scripts -->
    <script src="app.js"></script>
    <script src="authController.js"></script>
    <script src="userController.js"></script>
</head>
<body layout="column" ng-app="bizApp">
<md-toolbar>
    <h3 class="md-toolbar-tools">welcome</h3>
</md-toolbar>
<md-content class="md-padding">
    <div ui-view>
    </div>
</md-content>


</body>


</html>
