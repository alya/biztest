<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //generate 50 random user data from randomuser.me/api and seed to users table
        DB::table('users')->delete();

        DB::table('users')->insert([
            'name' => 'Mr. Admin',
            'email' => 'admin@example.com',
            'password' => Hash::make('secret'),
            'phone' => '0811129207',
            'pict' => 'https://randomuser.me/api/portraits/thumb/men/77.jpg',
        ]);

        for ($i=1;$i<50;$i++) {
            $curl = curl_init();
            curl_setopt ($curl, CURLOPT_URL, "https://randomuser.me/api/");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

            $result = (array) json_decode(curl_exec ($curl));
            curl_close ($curl);

            $user = (array) $result['results'][0];

            DB::table('users')->insert([
                'name' => $user['user']->name->title.' '.$user['user']->name->first.' '.$user['user']->name->last,
                'email' => $user['user']->email,
                'password' => Hash::make('secret'),
                'phone' => $user['user']->phone,
                'pict' => $user['user']->picture->thumbnail,
            ]);
        }
    }
}
